package stream.gpu;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;

import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngineManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.ProcessContext;
import stream.Processor;
import stream.StatefulProcessor;
import stream.annotations.Description;
import stream.data.Data;
import stream.util.URLUtilities;
import stream.script.*;

import static jcuda.driver.JCudaDriver.*;

import java.util.Arrays;

import jcuda.*;
import jcuda.runtime.*;
import jcuda.driver.*;
import stream.gpu.utils.*;

/**
 * @author nico
 * 
 */
@Description(group = "Data Stream.Processing.CUDA")
public class CUDA extends Script {
	static Logger log = LoggerFactory.getLogger(CUDA.class);

	final static ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
	final static String preamble = URLUtilities.readContentOrEmpty(CUDA.class.getResource("/stream/data/CUDA.preamble"));

	transient String theScript = null;
	String script = null;

	private KernelLauncher kernel = null;
	private CUdeviceptr gpuData = null;

	private int device = 0;
	private int ld = -1;

	Processor impl;

	/**
	 * @param engine
	 */
	public CUDA(){
        	JCudaDriver.setExceptionsEnabled(true);
	}

	/**
	 * @see stream.AbstractProcessor#init(stream.Context)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);
		script = "extern \"C\" __global__ void process(double* data){" + loadScript() + "}";
	}

	private void initDev(){
		ld = device;

		cudaDeviceProp p = new cudaDeviceProp();
		JCuda.cudaGetDeviceProperties(p, device);

		int l = 0;
		while(p.name[l++] != 0);

		log.info("Using CUDA device {}: {}", device, new String(p.name).substring(0,l-1));

		kernel = KernelLauncher.compile(device, script, "process");
	}

	@Override
	public void finish() throws Exception {
		JCuda.cudaDeviceReset();
	}

	/**
	 * @see stream.DataProcessor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data data){

		if(kernel == null || ld != device) initDev();

		double[] tempData = new double[data.size()];
		int i = 0;

		try {
			for(Serializable s : data.values())
				tempData[i++] = Double.parseDouble(s.toString());

		}catch(Exception e){
			log.error("Cannot convert DataItem to Double[]! {}", e.getMessage());
			return null;
		}

		if(gpuData == null) gpuData = new CUdeviceptr();

		cuMemAlloc(gpuData, data.size() * Sizeof.DOUBLE);
		cuMemcpyHtoD(gpuData, Pointer.to(tempData), data.size() * Sizeof.DOUBLE);

		kernel.setGridSize(1, 1, 1);
		kernel.setBlockSize(data.size(), 1, 1);
		kernel.call(gpuData);
		JCuda.cudaDeviceSynchronize();

		cuMemcpyDtoH(Pointer.to(tempData), gpuData, data.size() * Sizeof.DOUBLE);
		cuMemFree(gpuData);

		i = 0;
		for(String s : data.keySet())
			data.put(s, new Double(tempData[i++]));

		return data;
	}

	protected String loadScript() throws Exception {

		if (embedded != null) {
			log.info("Using embedded CUDA...");
			theScript = preamble + "\n" + embedded.getContent();
			return theScript;
		}

		if (file != null) {
			log.debug("Reading CUDA source from file {}", file);
			theScript = loadScript(new FileInputStream(file));
			return theScript;
		}

		throw new Exception("Neither embedded script nor file provided!");
	}

	protected String loadScript(InputStream in) throws Exception {
		log.debug("Loading script from input-stream {}", in);
		StringBuffer s = new StringBuffer();
		s.append(preamble);
		s.append("\n");
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line = reader.readLine();
		while (line != null) {
			s.append(line + "\n");
			log.debug("Appending line: {}", line);
			line = reader.readLine();
		}
		reader.close();
		return s.toString();
	}

	public void setDevice(String s){
		device = Integer.parseInt(s);
	}

	public int getDevice(){
		return device;
	}
}
