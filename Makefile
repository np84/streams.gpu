
MAVEN=mvn
SKIP_TESTS=true


all:
	@echo "Building stream-sandbox.jar"
	@mvn -DskipTests=$(SKIP_TESTS) assembly:assembly
	@cp target/stream-sandbox.jar .

clean:
	@echo "Cleaning project..."
	@mvn clean
